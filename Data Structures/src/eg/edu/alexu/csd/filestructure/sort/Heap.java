package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;
import java.util.Collection;

public class Heap<T extends Comparable<T>> implements IHeap<T> {

  private int size = 0;
  private ArrayList<INode<T>> heapArray = new ArrayList<>();

  private void swap(INode<T> a, INode<T> b) {
    T temp = a.getValue();
    a.setValue(b.getValue());
    b.setValue(temp);
  }

  public void sort() {
    if (size < 2)
      return;
    int temp = size;
    swap(heapArray.get(0), heapArray.get(size - 1));
    size--;
    while (size > 1) {
      heapify(heapArray.get(0));
      swap(heapArray.get(0), heapArray.get(size - 1));
      size--;
    }
    size = temp;
  }

  @Override
  public INode<T> getRoot() {
    if (size == 0) {
      throw new RuntimeException("empty heap");
    }
    return heapArray.get(0);
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public void heapify(INode<T> node) {
    INode<T> large = node;
    INode<T> left = node.getLeftChild();
    INode<T> right = node.getRightChild();
    if (left != null && left.getValue().compareTo(node.getValue()) > 0) {
      large = left;
    }
    if (right != null && right.getValue().compareTo(large.getValue()) > 0) {
      large = right;
    }
    if (large != node) {
      swap(large, node);
      heapify(large);
    }

  }

  @Override
  public void insert(T element) {

    INode<T> node = new Node(size++, element);
    heapArray.add(node);
    if (node.getParent() != null) {
      while (node.getParent() != null
          && node.getParent().getValue().compareTo(node.getValue()) < 0) {
        // swap parent and node
        swap(node.getParent(), node);
//        T temp = node.getParent().getValue();
//        node.getParent().setValue(node.getValue());
//        node.setValue(temp);
        node = node.getParent();
      }
    }

  }

  @Override
  public void build(Collection<T> unordered) {
    for (T element : unordered) {
      INode<T> node = new Node(size++, element);
      heapArray.add(node);
    }

    if (size > 1) {
      for (int i = (size - 1) / 2; i >= 0; i--) {
        heapify(heapArray.get(i));
      }
    }
  }

  @Override
  public T extract() {
    if (heapArray.size() == 0) {
      return null;
    }
    T max = heapArray.get(0).getValue();
    heapArray.get(0).setValue(heapArray.get(size - 1).getValue());
    heapArray.remove(size - 1);
    size--;
    if (heapArray.size() > 1) {
      heapify(heapArray.get(0));
    }
    return max;
  }

  private class Node implements INode<T> {

    private int index;
    private T value;

    public Node(int index, T value) {
      this.index = index;
      this.value = value;
    }

    @Override
    public INode<T> getLeftChild() {
      int leftChild = index * 2 + 1;
      if (leftChild >= size) {
        return null;
      }
      return heapArray.get(leftChild);
    }

    @Override
    public INode<T> getRightChild() {
      int rigthChild = index * 2 + 2;
      if (rigthChild >= size) {
        return null;
      }
      return heapArray.get(rigthChild);
    }

    @Override
    public INode<T> getParent() {
      if (index == 0) {
        return null;
      }
      int parent = index >> 1;
      return heapArray.get(parent);
    }

    @Override
    public T getValue() {
      return value;
    }

    @Override
    public void setValue(T value) {
      this.value = value;
    }

    public String toString() {
      return value.toString();
    }

  }

}
