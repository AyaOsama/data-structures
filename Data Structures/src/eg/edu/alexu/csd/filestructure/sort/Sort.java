package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;

public class Sort<T extends Comparable<T>> implements ISort<T> {
  
  @Override
  public IHeap<T> heapSort(ArrayList<T> unordered) {
    Heap<T> heap = new Heap<>();
    heap.build(unordered);
    heap.sort();
    return heap;
  }

  @Override
  public void sortSlow(ArrayList<T> unordered) {
    // bubble sort
    for (int i = 0; i < unordered.size() - 1; i++) {
      for (int j = 0; j < (unordered.size() - 1) - i; j++) {

        if (unordered.get(j).compareTo(unordered.get(j + 1)) > 0) {
          // swap
          T temp = unordered.get(j);
          unordered.set(j, unordered.get(j + 1));
          unordered.set(j + 1, temp);
        }
      }
    }

  }

  @Override
  public void sortFast(ArrayList<T> unordered) {
    // quick sort
    quickSort(unordered, 0, unordered.size() - 1);
  }

  private void quickSort(ArrayList<T> unordered, int p, int r) {
    if (p < r) {
      int q = partition(unordered, p, r);
      quickSort(unordered, p, q - 1);
      quickSort(unordered, q + 1, r);
    }
  }

  private int partition(ArrayList<T> unordered, int p, int r) {
    T xValue = unordered.get(r);
    int i = p - 1;
    for (int j = p; j < r; j++) {
      if (unordered.get(j).compareTo(xValue) <= 0) {
        i++;
        // swap
        T temp = unordered.get(j);
        unordered.set(j, unordered.get(i));
        unordered.set(i, temp);
      }
    }
    T temp = unordered.get(i + 1);
    unordered.set(i + 1, unordered.get(r));
    unordered.set(r, temp);

    return i + 1;

  }
}
