package eg.edu.alexu.csd.filestructure.graphs;

public class Vertex implements Comparable<Vertex> {

  private int number;
  private int value;

  public Vertex(int num, int val) {
    this.number = num;
    this.value = val;
  }
  
  public int getNumber(){
    return number;
  }
  
  public int getValue(){
    return value;
  }
  
  public void setValue(int newValue){
    this.value = newValue;
  }


  @Override
  public int compareTo(Vertex otherVertex) {
    
    return Integer.compare(value, otherVertex.value);
  }
  

}
