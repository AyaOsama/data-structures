package eg.edu.alexu.csd.filestructure.graphs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;

public class Graph implements IGraph {

  private static int INFINITY = Integer.MAX_VALUE / 2;
  private ArrayList<ArrayList<AdjacentNode>> adjList = new ArrayList<>();
  private ArrayList<Edge> edges = new ArrayList<>();
  private int numOfVertices;
  private int numOfEdges;

  @Override
  public void readGraph(File file) {

    try {
      FileReader fileReader = new FileReader(file);
      BufferedReader buffer = new BufferedReader(fileReader);
      String line = null;
      // read first line #of vertices and #of edges
      line = buffer.readLine();
        if (line != null) {
          String[] dummy = line.split(" ");
          numOfVertices = Integer.parseInt(dummy[0]);
          numOfEdges = Integer.parseInt(dummy[1]);
        }
        // initial adjList with number of vertices
        for (int i = 0; i < numOfVertices; i++) {
          ArrayList<AdjacentNode> dummy = new ArrayList<>();
          adjList.add(dummy);
        }

        // read edges
        int counter = 0;
        while ((line = buffer.readLine()) != null) {
          counter++;
          String[] dummy = line.split(" ");
          // add edges to list of edges
          if (Integer.parseInt(dummy[0]) >= numOfVertices
              || Integer.parseInt(dummy[0]) < 0
              || Integer.parseInt(dummy[1]) >= numOfVertices
              || Integer.parseInt(dummy[1]) <0) {
            buffer.close();
            throw new RuntimeException();
          }
          Edge e = new Edge(Integer.parseInt(dummy[0]),
              Integer.parseInt(dummy[1]), Integer.parseInt(dummy[2]));
          edges.add(e);

          // add to adjList
          AdjacentNode node = new AdjacentNode(Integer.parseInt(dummy[1]),
              Integer.parseInt(dummy[2]));
          adjList.get(Integer.parseInt(dummy[0])).add(node);
        }
        if (counter != numOfEdges) {
          buffer.close();
          throw new RuntimeException();
        }
      buffer.close();
    } catch (Exception e) {
      throw  new RuntimeException();
    }

  }

  @Override
  public int size() {
    return numOfEdges;
  }

  @Override
  public ArrayList<Integer> getVertices() {
    if (numOfVertices == 0) {
      return null;
    }
    ArrayList<Integer> vertices = new ArrayList<>();
    for (int i = 0; i < numOfVertices; i++) {
      vertices.add(i);
    }
    return vertices;
  }

  @Override
  public ArrayList<Integer> getNeighbors(int v) {
    if (numOfVertices == 0) {
      return null;
    }
    ArrayList<Integer> neighbors = new ArrayList<>();
    ArrayList<AdjacentNode> adjNodes = adjList.get(v);
    for (AdjacentNode n : adjNodes) {
      neighbors.add(n.getNumber());
    }
    return neighbors;
  }

  private ArrayList<Integer> dijkstraProcessedOrder = new ArrayList<>();

  @Override
  public void runDijkstra(int src, int[] distances) {

    if (numOfVertices == 0) {
      return;
    }
    boolean[] visited = new boolean[numOfVertices];
    PriorityQueue<Vertex> q = new PriorityQueue<>();

    Arrays.fill(distances, INFINITY);
    distances[src] = 0;

    q.add(new Vertex(src, 0));
    while (!q.isEmpty()) {
      Vertex v = q.poll();
      int num1 = v.getNumber();
      if (!visited[num1]) {
        dijkstraProcessedOrder.add(num1);
        visited[num1] = true;
        ArrayList<AdjacentNode> adjVertices = adjList.get(num1);
        for (AdjacentNode node : adjVertices) {
          int num2 = node.getNumber();
          int weight = node.getWeight();
          if (distances[num1] + weight < distances[num2]) {
            distances[num2] = distances[num1] + weight;
            q.add(new Vertex(num2, distances[num2]));
          }
        }
      }
    }
  }

  @Override
  public ArrayList<Integer> getDijkstraProcessedOrder() {

    if (numOfVertices == 0) {
      return null;
    }
    return dijkstraProcessedOrder;
  }

  @Override
  public boolean runBellmanFord(int src, int[] distances) {

    if (numOfVertices == 0) {
      return false;
    }
    Arrays.fill(distances, INFINITY);
    distances[src] = 0;
    boolean relax = true; // to improve time
    for (int i = 0; i < numOfVertices - 1 && relax; i++) {
      relax = false;
      for (Edge e : edges) {
        // relaxation
        if (distances[e.getFrom()] + e.getWeight() < distances[e.getTo()]) {
          relax = true;
          distances[e.getTo()] = distances[e.getFrom()] + e.getWeight();
        }
      }
    }

    // check negative cycles
    for (Edge e : edges) {
      if (distances[e.getFrom()] + e.getWeight() < distances[e.getTo()]) {
        return false;
      }
    }

    return true;
  }

}
