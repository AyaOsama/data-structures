package eg.edu.alexu.csd.filestructure.graphs;

public class AdjacentNode {
  private int number;
  private int weight;
  
  public AdjacentNode(int num , int weight) {
    this.number = num;
    this.weight =weight;
  }
  
  public int getNumber(){
    return number;
  }
  
  public int getWeight(){
    return weight;
  }

}
