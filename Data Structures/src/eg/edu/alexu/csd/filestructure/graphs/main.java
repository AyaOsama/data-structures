package eg.edu.alexu.csd.filestructure.graphs;

import java.io.File;
import java.util.ArrayList;
import java.util.PriorityQueue;

public class main {

  public static void main(String[] args) {
    
    PriorityQueue<Vertex> q = new PriorityQueue<>();
    q.add(new Vertex(1, 5));
    q.add(new Vertex(1, 3));
    q.add(new Vertex(2, 4));
    
    while(!q.isEmpty()){
      System.out.println(q.poll().getValue());
    }
    
    boolean [] visited = new boolean[2];
    System.out.println(visited[0]);

    q.add(new Vertex(1, 5));
    q.add(new Vertex(1, 3));
    
    while(!q.isEmpty()){
      System.out.println(q.poll().getValue());
    }

    
    
    

    
    
    IGraph g = new Graph();
    g.readGraph(new File("C:\\Users\\hp\\git\\aya-osama\\Data Structure\\src\\eg\\edu\\alexu\\csd\\filestructure\\graphs\\file.txt"));
    ArrayList<Integer> v = g.getVertices();
    System.out.println("V");
    for(Integer i:v){
      System.out.println(i);
    }
    
    System.out.println("E");
    ArrayList<Integer> e = g.getNeighbors(0);
    for(Integer i:e){
      System.out.println(i);
    }
    
    System.out.println("size" + g.size());
    
    System.out.println("-----------");
    int [] a = new int [g.size()];
    //System.out.println(g.runBellmanFord(0, a));
    g.runDijkstra(0, a);
    
    System.out.println("shortest path");
    for(int i : a){
      System.out.println(i);
    }
    
    System.out.println("---- order");
    ArrayList<Integer> list = g.getDijkstraProcessedOrder();
    for(Integer i : list){
      System.out.println(i);
    }
    
    

  }

}
