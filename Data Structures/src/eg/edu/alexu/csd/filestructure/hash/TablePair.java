package eg.edu.alexu.csd.filestructure.hash;

public class TablePair<K,V> {
  private K key;
  private V value;
  private boolean deleted;
  
  public TablePair(K key, V value) {
    this.key = key;
    this.value = value;
    this.deleted = false;
  }
  
  public void setKey(K key) {
    this.key = key;
  }

  public void setValue(V value) {
    this.value = value;
  }

  public K getKey() {
    return key;
  }

  public V getValue() {
    return value;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }
   
  

}
