package eg.edu.alexu.csd.filestructure.hash;

import java.util.LinkedList;

public class HashChaining<K, V> implements IHash<K, V>, IHashChaining {

  private LinkedList<TablePair>[] table;
  private int tableSize;
  private int capacity;
  private int collisions;
  private LinkedList<K> keyList;

  public HashChaining() {
    // initialize
    table = new LinkedList[1200];
    keyList = new LinkedList<>();
    tableSize = 0;
    capacity = 1200;
    collisions = 0;
    for (int i = 0; i < 1200; i++) {
      table[i] = new LinkedList<>();
    }
  }

  @Override
  public void put(K key, V value) {
    TablePair pair = new TablePair(key, value);
    int hashValue = key.hashCode() % table.length;

    if (table[hashValue].size() == 0) {
      capacity--;
    } else { // collisions
      collisions += table[hashValue].size();
    }
    table[hashValue].add(pair);
    tableSize++;
    keyList.add(key);

  }

  @Override
  public String get(K key) {
    int hashValue = key.hashCode() % table.length;
    if (table[hashValue].size() == 0) {
      return null;
    }
    for (int i = 0; i < table[hashValue].size(); i++) {
      if (table[hashValue].get(i).getKey().equals(key)) {
        return (String) table[hashValue].get(i).getValue();
      }
    }

    return null;
  }

  @Override
  public void delete(K key) {
    if (isEmpty()) {
      throw new RuntimeException("empty table!!");
    }
    int hashValue = key.hashCode() % table.length;
    for (int i = 0; i < table[hashValue].size(); i++) {
      if (table[hashValue].get(i).getKey().equals(key)) {
        table[hashValue].remove(i);
        if (table[hashValue].size() == 0) {
          capacity++;
        }
        tableSize--;
        keyList.remove(key);
      }
    }

  }

  @Override
  public boolean contains(K key) {
    int hashValue = key.hashCode() % table.length;
    for (int i = 0; i < table[hashValue].size(); i++) {
      if (table[hashValue].get(i).getKey().equals(key)) {
        return true;
      }
    }

    return false;
  }

  @Override
  public boolean isEmpty() {
    if (tableSize == 0) {
      return true;
    }
    return false;
  }

  @Override
  public int size() {
    return tableSize;
  }

  @Override
  public int capacity() {
    return capacity + tableSize;
  }

  @Override
  public int collisions() {
    return collisions;
  }

  @Override
  public Iterable<K> keys() {
    return keyList;
  }

}
