package eg.edu.alexu.csd.filestructure.hash;

import java.util.ArrayList;

public class HashDouble<K, V> implements IHash<K, V>, IHashDouble {

  private TablePair<K, V>[] table;
  private int tableCapacity;
  private int tableSize;
  private int collisions;
  private TablePair<K, V>[] order;

  @SuppressWarnings("unchecked")
  public HashDouble() {
    table = new TablePair[1200];
    tableSize = 0;
    collisions = 0;
    tableCapacity = 1200;
    order = new TablePair[19200];
  }

  @Override
  public void put(K key, V value) {
    for (int counter = 0; counter < tableCapacity; counter++) {
      int index = hash(key.hashCode(), counter);
      if (table[index] == null || table[index].isDeleted()) {
        TablePair<K, V> pair = new TablePair<K, V>(key, value);
        table[index] = pair;
        order[tableSize] = pair;
        tableSize++;
        return;
      } else {
        collisions++;
      }
    }
    rehash();
    collisions++;
    put(key, value);
    return;
  }

  @Override
  public String get(K key) {
    for (int counter = 0; counter < tableCapacity; counter++) {
      int index = hash(key.hashCode(), counter);
      if (table[index] == null) {
        return null;
      } else if (table[index].getKey().equals(key) && !table[index].isDeleted()) {
        return table[index].getValue().toString();
      }
    }
    return null;
  }

  @Override
  public void delete(K key) {

    for (int counter = 0; counter < tableCapacity; counter++) {
      int index = hash(key.hashCode(), counter);
      if (table[index] == null) {
        return;
      } else if (table[index].getKey().equals(key) && !table[index].isDeleted()) {
        table[index].setDeleted(true);
        tableSize--;
      }
    }
    return;
  }

  @Override
  public boolean contains(K key) {
    for (int counter = 0; counter < tableCapacity; counter++) {
      int index = hash(key.hashCode(), counter);
      if (table[index] == null) {
        return false;
      } else if (table[index].getKey().equals(key) && !table[index].isDeleted()) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean isEmpty() {
    return tableSize == 0;
  }

  @Override
  public int size() {
    return tableSize;
  }

  @Override
  public int capacity() {
    return tableCapacity;
  }

  @Override
  public int collisions() {
    return collisions;
  }

  @Override
  public Iterable<K> keys() {
    ArrayList<K> keys = new ArrayList<K>();
    for (int counter = 0; counter < table.length; counter++) {
      if (table[counter] != null && !table[counter].isDeleted())
        keys.add(table[counter].getKey());
    }
    return keys;
  }

  @SuppressWarnings("unchecked")
  private void rehash() {
    int oldSize = tableSize;
    tableCapacity *= 2;
    table = new TablePair[tableCapacity];
    tableSize = 0;
    for (int counter = 0; counter < oldSize; counter++) {
      TablePair<K, V> temp = order[counter];
      if (!temp.isDeleted())
        put(temp.getKey(), temp.getValue());
    }
  }

  private int hash(int key, int i) {
    return (h1(key) + h2(key) * i) % tableCapacity;
  }

  private int h1(int key) {
    return key % tableCapacity;
  }

  private int h2(int key) {
    return 1193 - key % 1193;
  }
}
