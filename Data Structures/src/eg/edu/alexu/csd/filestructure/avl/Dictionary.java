package eg.edu.alexu.csd.filestructure.avl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * The Class Dictionary.
 */
public class Dictionary implements IDictionary {

  /** The avl tree of string. */
  private IAVLTree<String> avl;
  
  /** The size of the dictionary. */
  private int size;

  /**
   * Constructor.
   */
  public Dictionary() {
    this.avl = new Avl<>();
    this.size = 0; // set size
  }

  
  @Override
  public void load(File file) {
    try {
      FileReader fileReader = new FileReader(file);
      BufferedReader buffer = new BufferedReader(fileReader);
      String line = null;
      while ((line = buffer.readLine()) != null) {
        this.insert(line); // each line contain one word to insert
      }
      buffer.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  
  @Override
  public boolean insert(String word) {
    if (avl.search(word)) { // no duplicate
      return false;
    }
    // not found , insert it
    avl.insert(word);
    size++; // increment size
    return true;
  }

  
  @Override
  public boolean exists(String word) {
    return avl.search(word);
  }

  
  @Override
  public boolean delete(String word) {
    if(avl.delete(word)){
      size--; // decrement size
      return true;
    }
    return false;
  }

  
  @Override
  public int size() {
    return size;
  }

 
  @Override
  public int height() {
    return avl.height();
  }

}
