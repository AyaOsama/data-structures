package eg.edu.alexu.csd.filestructure.avl;

/**
 * The Class Avl.
 *
 * @param <T> the generic type
 */
public class Avl<T extends Comparable<T>> implements IAVLTree<T> {

  /** The Constant ALLOWED_IMBALANCE. */
  private static final int ALLOWED_IMBALANCE = 1;
  
  /** The root. */
  private Node root;
  
  /** The deleted. */
  private boolean deleted;

 
  /**
   * Constructor.
   */
  public Avl() {
    this.root = null;
    this.deleted = true;
  }

  @Override
  public void insert(T key) {
    root = insert(key, root);
  }

  /**
   * Insert.
   *
   * @param key the key to insert
   * @param currentRoot the current root
   * @return the new root
   */
  private Node insert(T key, Node currentRoot) {
    if (currentRoot == null) { // create new node with given key
      return new Node(key);
    }

    int compareResult = key.compareTo(currentRoot.element);

    if (compareResult < 0) { // go to left
      currentRoot.left = insert(key, currentRoot.left);
    } else if (compareResult >= 0) { // go to right
      currentRoot.right = insert(key, currentRoot.right);
    }
    // check balance after insertion
    return balance(currentRoot);

  }

  
  @Override
  public boolean delete(T key) {
    root = delete(key, root);
    return deleted; // true if node deleted, false if not exists
  }

  /**
   * Delete.
   *
   * @param key the key to delete
   * @param currentRoot the current root
   * @return the new root
   */
  private Node delete(T key, Node currentRoot) {
    if (currentRoot == null) {
      deleted = false;
      return currentRoot; // Item not found; do nothing
    }

    int compareResult = key.compareTo(currentRoot.element);

    if (compareResult < 0) { // go to left
      currentRoot.left = delete(key, currentRoot.left);
    } else if (compareResult > 0) { // go to right
      currentRoot.right = delete(key, currentRoot.right);
    } else if (currentRoot.left != null && currentRoot.right != null) // Two
    // children
    {
      // found 
      deleted = true;
      currentRoot.setValue(findMin(currentRoot.right).getValue());
      currentRoot.right = delete(currentRoot.element, currentRoot.right);
    } else { // one child or no children
      // found
      deleted = true;
      if (currentRoot.left != null) {
        currentRoot = currentRoot.left;
      } else {
        currentRoot = currentRoot.right;
      }
    }
    // check balance after deletion
    return balance(currentRoot);
  }

  /**
   * Find min.
   *
   * @param currentRoot the current root
   * @return the node with min key
   */
  private Node findMin(Node currentRoot) {
    if (currentRoot == null) // empty tree
      return null;
    if (currentRoot.left == null) // found
      return currentRoot;
    return findMin(currentRoot.left);
  }

  
  @Override
  public boolean search(T key) {
    return contains(key, root);
  }

  /**
   * Contains.
   *
   * @param key the of the node
   * @param currentRoot the current root
   * @return true if the key exists, false otherwise
   */
  private boolean contains(T key, Node currentRoot) {
    if (currentRoot == null) // not found
      return false;

    int compareResult = key.compareTo(currentRoot.element);

    if (compareResult < 0) // go to left
      return contains(key, currentRoot.left);
    else if (compareResult > 0) // go to right
      return contains(key, currentRoot.right);
    else
      return true; // Match
  }

  // return height of the root
  @Override
  public int height() {
    if (root == null) { // empty tree
      return 0;
    }
    return root.height; 
  }

  // return the root of tree
  @Override
  public INode<T> getTree() {
    return root;
  }

  /**
   * Height.
   *
   * @param t the node
   * @return the height of the node
   */
  private int height(Node t) {
    if (t == null) {
      return 0;
    }
    return t.height;
  }

  /**
   * Balance.
   *
   * @param currentRoot the current root
   * @return the new root
   */
  private Node balance(Node currentRoot) {
    if (currentRoot == null) { // empty tree
      return currentRoot;
    }

    if (height(currentRoot.left) - height(currentRoot.right) > ALLOWED_IMBALANCE) { // not balanced
      if (height(currentRoot.left.left) - height(currentRoot.left.right) >= ALLOWED_IMBALANCE) {
        currentRoot = rotateWithLeftChild(currentRoot);
      } else {
        currentRoot = doubleWithLeftChild(currentRoot);
      }

    } else {
      if (height(currentRoot.right) - height(currentRoot.left) > ALLOWED_IMBALANCE) { // not balanced
        if (height(currentRoot.right.right) - height(currentRoot.right.left) >= ALLOWED_IMBALANCE) {
          currentRoot = rotateWithRightChild(currentRoot);
        } else {
          currentRoot = doubleWithRightChild(currentRoot);
        }
      }

    }

    // set height
    currentRoot.height = Math.max(height(currentRoot.left), height(currentRoot.right)) + 1;
    return currentRoot;
  }

  /**
   * Rotate with left child.
   *
   * @param k2 the old root
   * @return the new root
   */
  private Node rotateWithLeftChild(Node k2) {
    Node k1 = k2.left;
    k2.left = k1.right;
    k1.right = k2;
    k2.height = Math.max(height(k2.left), height(k2.right)) + 1; // set height
    k1.height = Math.max(height(k1.left), k2.height) + 1; // set height
     
    return k1;
  }

  /**
   * Rotate with right child.
   *
   * @param k2 the old root
   * @return the new root
   */
  private Node rotateWithRightChild(Node k2) {
    Node k1 = k2.right;
    k2.right = k1.left;
    k1.left = k2;
    k2.height = Math.max(height(k2.left), height(k2.right)) + 1; // set height
    k1.height = Math.max(height(k1.right), k2.height) + 1; // set height
    return k1;
  }

  /**
   * Double with left child.
   *
   * @param k3 the old root
   * @return the new root
   */
  private Node doubleWithLeftChild(Node k3) {
    k3.left = rotateWithRightChild(k3.left);
    return rotateWithLeftChild(k3);
  }

  /**
   * Double with right child.
   *
   * @param k3 the old root
   * @return the new root
   */
  private Node doubleWithRightChild(Node k3) {
    k3.right = rotateWithLeftChild(k3.right);
    return rotateWithRightChild(k3);
  }

  // for print tree
  private int layout(Node p, int depth, int xCoordinate) {
    if (p == null) {
      return 0;
    }
    if (p.getLeftChild() != null) {
      xCoordinate = layout(p.left, depth + 1, xCoordinate); // resulting x
                                                            // will be
    }
    // increased
    xCoordinate = xCoordinate + 2;
    p.xCoordinate = xCoordinate; // post-increment x
    p.yCoordinate = depth;
    if (p.getRightChild() != null) {
      xCoordinate = layout(p.right, depth + 1, xCoordinate); //
    }
    return xCoordinate;
  }

  /**
   * Prints the tree.
   */
  public void printTree() {
    layout(root, 0, 0);
    StringBuilder[] builder = new StringBuilder[50];
    for (int counter = 0; counter < 50; counter++) {
      builder[counter] = new StringBuilder(
          "                                                                                       "
              + "                                                                                ");
    }
    inorderAdd(builder, root);
    System.out.println("*****************Treeeeeeeee*************************");
    for (int counter = 0; counter < 10; counter++) {
      System.out.println(builder[counter]);
    }

  }

  /**
   * Inorder add.
   */
  private void inorderAdd(StringBuilder[] builder, Node node) {

    if (node == null) {
      return;
    }
    inorderAdd(builder, node.left);
    String element = node.toString();
    for (int counter = 0; counter < element.length(); counter++) {
      builder[node.yCoordinate].setCharAt(node.xCoordinate + counter,
          element.charAt(counter));
    }
    inorderAdd(builder, node.right);
  }
  
  
  /**
   * The Class Node.
   */
  private class Node implements INode<T> {
    
    /** The left child. */
    private Node left;
    
    /** The right child. */
    private Node right;
    
    /** The element. */
    private T element;
    
    /** The height. */
    private int height;
    
    /** The x coordinate. */
    private int xCoordinate;
    
    /** The y coordinate. */
    private int yCoordinate;

    /**
     * Constructor.
     *
     * @param value of the node
     */
    // Constructor
    public Node(T value) {
      this.element = value;
      this.left=null;
      this.right=null;
      this.height=1;
    }

    
    @Override
    public INode<T> getLeftChild() {
      return left;
    }

    
    @Override
    public INode<T> getRightChild() {
      return right;
    }

    
    @Override
    public T getValue() {
      return element;
    }

    
    @Override
    public void setValue(T value) {
      element = value;
    }
    
    // for printing
    public String toString(){
      return element.toString();
    }

  }


}
